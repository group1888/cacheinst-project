This project contains Powershell-Scripts to 
- manage Caché
- install reference environment for developers

Scripts Caché reference environment:
- Get-RepositoryFile.ps1
- SYS_CACHE_INSTALL.ps1
- SYS_CACHE_REMOVE.ps1
- SYS_CACHE_UPDATE.ps1
- config.json