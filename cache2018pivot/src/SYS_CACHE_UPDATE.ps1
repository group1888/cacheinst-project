﻿# SYS_CACHE_UPDATE.ps1 / Update reference environment / HHLA - Iten / 2019-04-17
# UserInterface: IP 3 / Bastian Hornung
# Version 202011-1
#
# Documentation:
# Step 1 : Updates the Caché-Instance with a new Caché version
#          Only reasonable if there is a new setup file from Intersystems
# Step 2 : Update files
#          - *.ps1 in ref/cache/scripts
#          - Release-Library
# Step 3 : Update %-Objects if requested
# Step 4 : Directory consistency check.

# History
# 2019.08-1
# - Another UserInterface
# - Update ReleaseBuilder
#
# 201908-2
# - Don't distinguish between filenames when updating release library
#
# 202002-1
# - Use artifactory to download sources instead of net path I
#
# 202009-1
# - Download Caché-setup file from artifactory if update is requested
# - use config.json to estimate relevant files
#
# 2020-11
# - Update license file
#--------------------------------------------------------------------------------
#----- Functions ----------------------------------------------------------------

function Show-Intro
{
	$lineBorder = "-"*104
	$lineSpace = " "*104
	Write-Host
	Write-Host "+$lineBorder+"
	Write-Host "|$lineSpace|"
	Write-Host -ForegroundColor White "|                         ▄▄▄  ▄▄   ▄▄▄ ▄  ▄ ▄▄█▄    ▄  ▄ ▄▄▄  ▄▄▄   ▄▄  ▄▄▄▄ ▄▄▄▄                       |"
	Write-Host -ForegroundColor White "|                        █    █  █ █    █  █ █       █  █ █  █ █  █ █  █  █   █                          |"
	Write-Host -ForegroundColor White "|                        █    █▀▀█ █    █▀▀█ █▀▀     █  █ █▀▀  █  █ █▀▀█  █   █▀▀                        |"
	Write-Host -ForegroundColor White "|                         ▀▀▀ ▀  ▀  ▀▀▀ ▀  ▀ ▀▀▀▀    ▀▀▀▀ ▀    ▀▀▀  ▀  ▀  ▀   ▀▀▀▀                       |"
	Write-Host "|$lineSpace|"
	Write-Host "+$lineBorder+"
	Write-Host
	Write-Host "  STEPS: (1) Update Caché Installation (if a new Caché version exists)"
	Write-Host "         (2) Update Caché Scripts and Release environment (ref/cache/scripts/*.ps1 and ...Release\LIB\*.*)"
	Write-Host "         (3) Update HHLA-Objects (%-Routines and -Classes including Releasebuilder)"
	Write-Host "         (4) Consistency Check"
	Write-Host
	Write-Host  -NoNewLine                                                "  "
	Write-Host                                    -BackgroundColor DarkBlue "                                   "
	Write-Host  -NoNewLine                                                "  "
	Write-Host             -ForegroundColor White -BackgroundColor DarkBlue "  USAGE                            "
	Write-Host  -NoNewLine                                                "  "
	Write-Host                                    -BackgroundColor DarkBlue "                                   "
	Write-Host  -NoNewLine                                                "  "
	Write-Host  -NoNewLine -ForegroundColor Cyan  -BackgroundColor DarkBlue "  SPACE or ← →   "
	Write-Host             -ForegroundColor White -BackgroundColor DarkBlue                  "switch            "
	Write-Host  -NoNewLine                                                "  "
	Write-Host  -NoNewLine -ForegroundColor Cyan  -BackgroundColor DarkBlue "  RETURN         "
	Write-Host             -ForegroundColor White -BackgroundColor DarkBlue                  "confirm           "
	Write-Host  -NoNewLine                                                "  "
	Write-Host  -NoNewLine -ForegroundColor Cyan  -BackgroundColor DarkBlue "  Y or N         "
	Write-Host             -ForegroundColor White -BackgroundColor DarkBlue                  "confirm directly  "
	Write-Host  -NoNewLine                                                "  "
	Write-Host  -NoNewLine -ForegroundColor Cyan  -BackgroundColor DarkBlue "  ESC            "
	Write-Host             -ForegroundColor White -BackgroundColor DarkBlue                  "abort             "
	Write-Host  -NoNewLine                                                "  "
	Write-Host                                    -BackgroundColor DarkBlue "                                   "
	Write-Host
}

# Gibt die mitgegebene Frage mitsamt den beiden Antwortmoeglichekiten 'Yes' und 'No' aus.
# Die SelectedOption definiert, ob und welche der Antworten farblich hervorgehoben wird.
function Show-YesNoQuestion
{
    param(
		# Ja-Nein-Frage, die ausgegeben werden soll
		[Parameter(Mandatory=$true)] [String] $Prompt, 
		# 'Yes', 'No', 'CANCELLED'
		[String] $SelectedOption
	)

    Write-Host -NoNewLine "`r$Prompt" 
	
	If ("$SelectedOption" -eq 'Yes') {
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor Green                           "[ "
		Write-Host -NoNewLine -ForegroundColor Black -BackgroundColor White    " YES " 
		Write-Host -NoNewLine -ForegroundColor Green                           " ]"
		                                                                       
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor Green                           "  "
		Write-Host -NoNewLine -ForegroundColor Black -BackgroundColor DarkGray " NO " 
		Write-Host -NoNewLine -ForegroundColor Green                           "  "
	} elseif("$SelectedOption" -eq 'No')  {                                    
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor Green                           "  "
		Write-Host -NoNewLine -ForegroundColor Black -BackgroundColor DarkGray " YES " 
		Write-Host -NoNewLine -ForegroundColor Green                           "  "
		                                                                       
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor Green                           "[ "
		Write-Host -NoNewLine -ForegroundColor Black -BackgroundColor White    " NO " 
		Write-Host -NoNewLine -ForegroundColor Green                           " ]"
	} elseif("$SelectedOption" -eq 'CANCELLED')  {                             
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor Black -BackgroundColor Gray     " CANCELLED " 
	} else {                                                                   
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor White                           "  "
		Write-Host -NoNewLine -BackgroundColor DarkGray                        " YES " 
		Write-Host -NoNewLine -ForegroundColor White                           "  "
                                                                               
		Write-Host -NoNewLine                                                  " "
		                                                                       
		Write-Host -NoNewLine -ForegroundColor White                           "  "
		Write-Host -NoNewLine -BackgroundColor DarkGray                        " NO " 
		Write-Host -NoNewLine -ForegroundColor White                           "  "
	}
	Write-Host -NoNewLine " "
}


# Gibt einen der Strings 'Yes', 'No', 'CANCELLED' zurück
function Get-YesNoQuestion
{
	param(
		# Ja-Nein-Frage, die ausgegeben werden soll
		[Parameter(Mandatory=$true)] [String] $Prompt, 
		# 'Yes', 'No', 'CANCELLED'
		[String] $SelectedOption
	)
	
	Show-YesNoQuestion -Prompt $Prompt -SelectedOption $SelectedOption
	$DecisionHasBeenMade = $FALSE
	while (-Not ($DecisionHasBeenMade)) {
		$KeyPress = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		switch ($KeyPress.VirtualKeyCode) {
			# arrow left
			'37' { $SelectedOption = 'Yes' }
			# arrow right
			'39' { $SelectedOption = 'No' }
			# space bar
			'32' { If ($SelectedOption -eq 'Yes') { $SelectedOption = 'No' } else { $SelectedOption = 'Yes' } }
			# Y
			'89' { $SelectedOption = 'Yes'; $DecisionHasBeenMade = $TRUE }
			# N
			'78' { $SelectedOption =  'No'; $DecisionHasBeenMade = $TRUE }
			# ESCAPE
			'27' { $DecisionHasBeenMade = $TRUE; $SelectedOption =  'CANCELLED' }
			# RETURN
			'13' { $DecisionHasBeenMade = $TRUE }
		}
		Show-YesNoQuestion -Prompt $Prompt -SelectedOption $SelectedOption
		#Write-Host "Gedrückte Taste: $KeyPress"
		#Write-Host -NoNewLine "KeyPress.VirtualKeyCode: "$KeyPress.VirtualKeyCode
		#Write-Host "KeyPress.Character: "$KeyPress.Character
	}
	Show-YesNoQuestion -Prompt $Prompt -SelectedOption $SelectedOption
	Write-Host
	Write-Host
	If ("$SelectedOption" -eq 'CANCELLED') { exit }
	return $SelectedOption
}

function Update-Item {
    Param (
        [Parameter(Mandatory=$true)]
        [String] $fileSource,
        [Parameter(Mandatory=$true)]
        [String] $fileTarget
	)
	Copy-Item -Path $fileSource -Destination $fileTarget
    if (Test-path $fileTarget) {
        $creationDateCurrent = (Get-ChildItem -Path $fileTarget).LastWriteTime
		$creationDateSource = (Get-ChildItem -Path $fileSource).LastWriteTime
        if ($creationDateCurrent -eq $creationDateSource) {
			Write-Host "Update of $fileTarget succeeded"
        }
        else {
            Write-Host "Error upgrading $fileTarget"
        }
	}
}

function Update-Files {
    Param (
        [Parameter (Mandatory=$true)]
		[System.Object] $fileObject,
		[Parameter (Mandatory=$true)]
		[String] $pathSource,
		[Parameter (Mandatory=$true)]
		[String] $pathTarget
	)
	if (($pathSource.Substring($pathSource.Length-1)) -ne '\') {
		$pathSource = $pathSource + '\'
	}
	if (($pathTarget.Substring($pathTarget.Length-1)) -ne '\') {
		$pathTarget = $pathTarget + '\'
	}
    $scriptSource = $pathSource + $fileObject.Name
	$scriptTarget = $pathTarget + $fileObject.Name
    if (-Not (Test-Path $scriptTarget)) {
        Write-Host "Copy new file from $scriptSource to $scriptTarget"
        Update-Item -fileSource $scriptSource -fileTarget $scriptTarget
    }
    else {
        $creationDateSource = (Get-ChildItem -Path $scriptSource).LastWriteTime
		$creationDateTarget = (Get-ChildItem -Path $scriptTarget).LastWriteTime
        if ($creationDateSource -ne $creationDateTarget) {
            Write-Host "Updating $scriptTarget"
            Remove-Item -Path $scriptTarget -Force
            if (Test-Path $scriptTarget) {
                Write-Host -NoNewLine "Error deleting old version of "
				Write-Host -ForegroundColor Yellow "$scriptTarget"
			}
			else {
				Update-Item  -fileSource $scriptSource -fileTarget $scriptTarget
			}
        }
	}
}

#----- Script area --------------------------------------------------------------
#see SYS_CACHE_INSTALL.ps1 for details about this code
$CACHE_CONFIG = $env:Computername.split('-')[0]
# Assumes that script is called in D:\Referenzumgebung-Cache\ref
$REF_PATH = 'cache'
$SCRIPT_PATH = Join-Path -Path $REF_PATH -ChildPath 'scripts'
$UPGRADE_EXE = Join-Path -Path $REF_PATH -ChildPath 'cache-*win_x64.exe'
$DATA_BASE = Join-Path -Path 'D:\' -ChildPath $CACHE_CONFIG
$EXTERNAL_SOURCES = Join-Path -Path $DATA_BASE -ChildPath 'externalSources'
$RELEASE_LIB = Join-Path -Path (Join-Path $DATA_BASE -ChildPath 'Release') -ChildPath 'LIB'
$CONFIG_FILE = Join-Path -Path $REF_PATH -ChildPath 'config.json'
$CMD_GETFILES = Join-Path -Path $REF_PATH -ChildPath 'Get-RepositoryFile.ps1'
#C:\Intersystems\Cache\$CACHE_CONFIG
$INSTALL_DIR = Join-Path -Path (Join-Path -path (Join-Path -Path 'C:' -ChildPath 'Intersystems')-ChildPath 'cache') -ChildPath $CACHE_CONFIG
$installFiles = Get-Content -raw -Path $CONFIG_FILE | ConvertFrom-JSON
Show-Intro

# STEP 1

$SelectedOption = Get-YesNoQuestion -Prompt '(1/4) Update local Caché installation?' -SelectedOption 'No'
If ("$SelectedOption" -eq 'Yes') {
	$UPGRADE_EXE_CURRENT = Join-Path -Path $REF_PATH -ChildPath (Get-ChildItem -Path $UPGRADE_EXE).Name
	if ("$REF_PATH\" -ne $UPGRADE_EXE_CURRENT) {  #if no installation file exists, then UPGRADE_EXE_CURRENT == REF_PATH
		Remove-Item $UPGRADE_EXE_CURRENT
	}
	$cacheInstallationFile = $installFiles.files.installation
	$errorFound = & $CMD_GETFILES $cacheInstallationFile $REF_PATH
	if (-not $errorFound) {
		$UPGRADE_EXE = Join-Path -Path $REF_PATH -ChildPath (Get-ChildItem -Path $UPGRADE_EXE).Name
		if ($null -eq $UPGRADE_EXE) {
			Write-Host "Upgrade file not found on local system`nAborting update"
			exit 1
		}
	}
	else {
		Write-Host "Error copying setup file from repository`nAborting update"
		exit 1
	}
    if (Test-Path $UPGRADE_EXE) {
		# Starts an unattended installation / upgrade / reinstall of a new instance of Caché
		# see: https://cedocs.intersystems.com/latest/csp/docbook/DocBook.UI.Page.cls?KEY=GCI_windows#GCI_windows_silentinst
		Write-Host "Upgrading $CACHE_CONFIG"
		Start-Process -Wait -Verb runas -FilePath $UPGRADE_EXE -ArgumentList "/instance $CACHE_CONFIG",'/qb'
		Write-Host 'Installation done'
		Start-Process $INSTALL_DIR\bin\csystray.exe -NoNewWindow
    }
    else {
        Write-Host "Could not update Caché-installation:"
		Write-Host "$UPGRADE_EXE not found on this system`nAborting update"
		exit 1
    }
}

# STEP 2
Write-Host
Write-Host
Write-Host '(2/4) Updating scripts and files'
if (-not (Test-Path $EXTERNAL_SOURCES)) {
	$null = New-Item $EXTERNAL_SOURCES -ItemType Directory
}
else {
	if (((Get-ChildItem $EXTERNAL_SOURCES).Count) -gt 0) {
		Remove-Item $EXTERNAL_SOURCES\*   #remove old files in this directory
	}
}

$filesArray=$installFiles.files.hlamua,$installFiles.files.mustang

#
# several sources are organized in an array
# CacheWindowsReleaseBuilder\*.xml is stored into external sources in order to import the classes during instance creation

foreach ($fileName in $filesArray) {
	$errorFound = & $CMD_GETFILES $fileName $EXTERNAL_SOURCES
	if ($errorFound) {
    	Write-Host "Error during getting sources.`nFile: $fileName`nAborting installation"
    	exit 1
	}
}

if (-not (Test-Path $SCRIPT_PATH)) {
    $null = New-Item -Path $SCRIPT_PATH -ItemType Directory
}
Get-ChildItem -Path $EXTERNAL_SOURCES -Filter *.ps1 | ForEach-Object { Update-Files -FileObject $_ -pathSource $EXTERNAL_SOURCES -pathTarget $SCRIPT_PATH}

if (-not (Test-Path $RELEASE_LIB)) {
	$null = New-Item -Path $RELEASE_LIB -ItemType Directory
}
$fileRelease = $installFiles.files.release
$errorFound = & $CMD_GETFILES $fileRelease $RELEASE_LIB
if ($errorFound) {
	Write-Warning "Error updating release builder in $RELEASE_LIB"
}
# Update license key if a new one is stored in master reference
$FILE_LICENSE = Join-Path -Path $REF_PATH -ChildPath 'cache.key' 
$FILE_LICENSE_OBJECT = Get-ChildItem $FILE_LICENSE #we need an object to use the method which updates the files
$PATH_TARGET = Join-Path -Path $INSTALL_DIR -ChildPath 'MGR'
Update-Files $FILE_LICENSE_OBJECT $REF_PATH $PATH_TARGET

# STEP 3
Write-Host
Write-Host
$SelectedOption = Get-YesNoQuestion -Prompt '(3/4) Update HHLA-%-Objects?' -SelectedOption 'Yes'
If ("$SelectedOption" -eq 'Yes') {
	Write-Host "Updating HHLA-%-Objects...`n"
	Write-Host "Loading routines"
	& csession $CACHE_CONFIG -U %SYS "##class(de.hhla.sysman.Installer).importHHLA(""""""""""""""""HLAMUA"""""""""""""""")"
    & csession $CACHE_CONFIG -U HLAMUA "##class(%de.hhla.cache.util.Objects).importDirectory(""""""""""""""""HLAMUA"""""""""""""""")"
    $null = Remove-Item -Path $EXTERNAL_SOURCES -Recurse
}

# STEP 4
Write-Host
Write-Host
Write-Host '(4/4) Consistency Check'
Write-Host "Performing a consistency check on D:\$CACHE_CONFIG\"
$itemCreated = $false
$DATA_BASEArray="CacheSave\Log","ERROR","FTP","LOG","POOL","Release\LIB","Release\TMP","SAV","TRANSFER","UnitTest"
for ($i=0; $i -lt $DATA_BASEArray.Length; $i++) {
    $currentItem=$DATA_BASE + '\' + $DATA_BASEArray[$i]
    if (-Not(Test-Path $currentItem)) {
        $null = New-Item -Path $currentItem -ItemType Directory
        $itemCreated = $true
        Write-Host "$currentItem` created"
    }
}
if (-not $itemCreated) {
    Write-Host "Consistency check performed successfully."
}

Write-Host "`nUpdate-Script has finished`n"
exit $LASTEXITCODE