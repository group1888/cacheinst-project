﻿# SYS_CACHE_INSTALL.ps1 / Install Caché automatically / HHLA - Iten - 2018-06-07
# Version 202103-2
#
#----------------------------------------------------------------------------------------------------------------------
# Change log:
# 201902-1.3 : 
# Create directory C:\Scripts if it doesn't exist
# Copy *.ps1 scripts from ${reference}\externalSources\ into C:\Scripts
#
# 201903-1.1
# Error correction : Path-Variable not set (path was set but in adminC-name - environment)
#
# 201904-1.1
# Run installation script as normal user instead of adminC-username
# 
# 201905-1-1
# Use relative path for referencing reference environment instead of using D:Referenzumgebung-Cache... or R:\ref
# Define constants for file references
#
# 201908-1
# Creating default AD-User
# Copy release-builder files into target
#
# 201908-2
# Error correction: Copy all release-builder files into release library (old version copied only *.M, *.GOF and de.hhla.common.Releasebuilder.xml)
# Define environment variable CACHE_DB in machine context (reason: Caché can't access variables in user context using $SYSTEM.Util.GetEnviron(var))
#
# 202001-1
# Use artifactory to download sources instead of net path I
#
# 202009-1
# Get installation exe out of artifactory if it doesn't exist already exist in reference environment
# Only one argument needed to install Caché
# Manage external files in config.json
# Removal of cloud test-system since all files have been moved into GitLab. Tests will be done on local system
#
# 202103-1
# Copy some files out of reference environment into externalSources directory in order to get rid of old reference path values
#
# 202103-2
# Using InstallerDeveloper.xml instead of DefaultInstallerClass.xml
# Class separation because we also have installers for DIVA and LPL. So we don't have to rename the class every time when it has changed.
#
#----------------------------------------------------------------------------------------------------------------------
# Further documentation:
# Filestructure and names of variables
#
# C:\Intersystems\Cache                   -> $CACHE_BASE
# D:\                                     -> $DATA_BASE
# D:\${CCFG}                              -> $DATA_DIR
# D:\${CCFG}\externalSources              -> $DEST_DIR
# D:\${CCFG}\Release\LIB                  -> $RELEASE_DIR
# D:\<reference-path>\cache               -> $REFERENCE_PATH (given in arg[0])
# D:\<reference-path>\cache\log           -> $LOG_DIR
#---- FUNCTIONS ------------------------------------------------------------------------------ 
function Get-TimeStamp {
    return Get-Date -Format dd.MM.yyyy' 'HH:mm:ss
}

function Get-Password {
    return -join(0..32|ForEach-Object {[char][int]((65..90) + (97..122) + (48..57) + (35,36,42,43,44,45,46,47,59,61,63,64,91,92,93,95,123,125,126) | Get-Random)})  #taken out of https://blogs.technet.microsoft.com/heyscriptingguy/2015/11/05/generate-random-letters-with-powershell/
}

#---- SCRIPT AREA -----------------------------------------------------------------
if ($args.count -lt 1) {
    Write-Host "`nArguments not specified correctly"
    Write-Host " arg1 = Path of Installer-Manifest`n"
    exit 1
}

#Arguments:
$REFERENCE_PATH = $args[0]   #Path to reference environment

#Validate input
if (-not (Test-Path $REFERENCE_PATH)) {
    Write-Host "$REFERENCE_PATH doesn't exist"
}

$INSTALL_CLASS = Join-Path -Path $REFERENCE_PATH -ChildPath 'InstallerDeveloper.xml'
if (-Not(Test-Path $INSTALL_CLASS)) {
    Write-Host "`nNo manifest file found in path $REFERENCE_PATH"
    Write-Host "File expected: $INSTALL_CLASS`n"
    Write-Host "Exiting installation`n"
    exit 1
}

#Estimate a caché instance name from computername
#A caché instance may not contain special characters
#HHLA-Nodenames contain a hyphen which must be separated
#So we pick the first element Lnumber out of computername
$CACHE_CONFIG = $env:Computername.split('-')[0]
Write-Host "`nInstallation of Caché-Instance $CACHE_CONFIG`n"
Write-Host "$(Get-TimeStamp) Performing some pre activities"

#Installation base of Caché database
$CACHE_BASE = 'C:\Intersystems\Cache\'
#Base volume of data files
$DATA_BASE = 'D:\'
if (-not (Test-Path $DATA_BASE)) {
    Write-Host "No second partition (D:\) available`nExiting installation`n"
    exit 1
}
#Root for data directory (D:\$configname)
$DATA_DIR = $DATA_BASE + $CACHE_CONFIG 
#Destination directory of copied files (D:\$configname\externalSources)
$DEST_DIR = Join-Path -Path $DATA_DIR -ChildPath externalSources
#Path for external scripts (.\cache\scripts)
$SCRIPT_BASE = Join-Path -Path 'cache\' -ChildPath scripts  
#Installation directory of caché database (C:\Intersystems\cache\$configname)
$INSTALL_DIR = $CACHE_BASE + $CACHE_CONFIG 
#Path to installation log file (.\cache\Log)                          
$LOG_DIR = Join-Path -Path $REFERENCE_PATH -ChildPath 'Log' 
#Name of installation log file (VOL:\PATH\Log\Install\$configname_yyyymmddhhmmss.LOG)
$INSTALL_LOG = $LOG_DIR + '\Install_' + $CACHE_CONFIG + '_' + (Get-Date -Format yyyyMMddHHmmss) + '.LOG'              
#Directory for release installer (D:\$configname\Release\Lib)
$RELEASE_DIR = Join-Path -Path $DATA_DIR -ChildPath Release\Lib  
#Name of temporary installation file (D:\$configname\installCmd.ps1)
$TMP_CMDFILE = Join-Path -Path $DATA_DIR -ChildPath 'installCmd.ps1'
$LOG_LEVEL = 3
$GET_FILE = Join-Path $REFERENCE_PATH -ChildPath '\Get-RepositoryFile.ps1'
$CONFIG_FILE = Join-Path -Path $REFERENCE_PATH -ChildPath 'config.json'
if (-not (Test-Path $CONFIG_FILE)) {
    Write-Host "Configuration file $CONFIG_FILE not found`nExiting installation`n"
    exit 1
}

#check if caché config already exists
if (Test-Path $INSTALL_DIR`/mgr) {
    Write-Host "Caché-Instance $CACHE_CONFIG already exists`n"
    exit 1
}

if (-Not(Test-Path $LOG_DIR)) {
    $null = New-Item -ItemType Directory -Path $LOG_DIR 
}

#Sometimes there are circumstances where a logfile couldn't be created. So we check if a file can be created. Because it is a temporary test, the file created is deleted
$null = New-Item $INSTALL_LOG
if (-Not (Test-Path $INSTALL_LOG)) {
    Write-Host "Error creating logfile $INSTALL_LOG`n"
    exit 1
}
Remove-Item $INSTALL_LOG

# a flag that indicates an error
$errorFound=$false

# preparing destination directory to copy sources into
# we want to have a clean destination, so we delete existing elements
# although this is done in sys_cache_remove.ps1, nobody knows what happened during removal and installation.
if (Test-Path $DEST_DIR) {
    Write-Host "Removing old files in $DEST_DIR`n"
    remove-item -Path $DEST_DIR\*.* -Recurse
}
else {
        $null = New-Item -Path $DEST_DIR -ItemType Directory
}

# storing current ad-user into a file in order to create a Caché user named with ad-user to have single sign on into Caché
Set-Content $DEST_DIR`\currentUser.txt $env:USERNAME 
if (-not (Test-Path $DEST_DIR`\currentUser.txt)) {
    Write-Host "Error saving current User-Context $env:USERNAME"
    Write-Warning "Aborting installation`n"
    exit 1
}

# Estimating filenames out of configuration file
$installationFiles = Get-Content -raw -Path $CONFIG_FILE | ConvertFrom-JSON

# copy installation file from artifactory into reference environment
$INSTALL_EXE = (Get-ChildItem $REFERENCE_PATH`\cache-*win_x64.exe).FullName
if ($null -eq $INSTALL_EXE) {
    Write-Host "No setup file on local environment found, downloading setup file`n"
    $installSource = $installationFiles.files.installation
    $errorFound = & $GET_FILE $installSource $REFERENCE_PATH
    if (-not $errorFound) {
        $INSTALL_EXE = (Get-ChildItem $REFERENCE_PATH`\cache-*win_x64.exe).FullName
    }
    else {
        Write-Host "Error copying setup file aborting installation"
        exit 1
    }
}

#Error during copy 
if ($null -eq $INSTALL_EXE) {
    Write-Host "Installation file not found`nAborting installation"
    exit 1
}

#Copy finished but left something unusable
if (-not (Test-Path $INSTALL_EXE)) {
    Write-Host "Installation file $INSTALL_EXE not found`nAborting installation"
    exit 1
}

$filesArray = $installationFiles.files.hlamua,$installationFiles.files.mustang
foreach ($file in $filesArray) {
    $errorFound = & $GET_FILE $file $DEST_DIR
    if ($errorFound) {
        Write-Host "Error getting $file`nAborting installation`n"
        exit 1
    }
}

Write-Host "`n$(Get-TimeStamp) Pre activities done, installing Caché $CACHE_CONFIG`n"
Write-Host "Installation directory: $INSTALL_DIR"
Write-Host "Log info              : $INSTALL_LOG`n"
Write-Host "$(Get-TimeStamp) Starting installation"

$USER_FILES = $installationFiles.files.users,$installationFiles.files.tasks,$installationFiles.files.locale,$installationFiles.files.license
foreach ($file in $USER_FILES) {
    $fileSource = Join-Path -Path $REFERENCE_PATH -ChildPath $file
    $fileTarget = Join-Path -Path $DEST_DIR -ChildPath $file
    Copy-Item $fileSource $fileTarget
}

try {
    Set-Content $TMP_CMDFILE "Start-Process $INSTALL_EXE -ArgumentList ""/instance  $CACHE_CONFIG /qn INSTALLDIR=$INSTALL_DIR INSTALLERMANIFEST=$INSTALL_CLASS INSTALLERMANIFESTLOGLEVEL=$LOG_LEVEL INSTALLERMANIFESTLOGFILE=$INSTALL_LOG UNICODE=1 INITIALSECURITY=Normal CACHEUSERPASSWORD=$(Get-Password)"""
    Add-Content $TMP_CMDFILE "[System.Environment]::SetEnvironmentVariable('CACHE_DB','$DATA_DIR`\','machine')"
}
catch {
    Write-Host "Error creating temporary command file`nDebug: $INSTALLER_MANIFEST_PARAMS`nAborting installation"
    exit 1
}
if (Test-Path $TMP_CMDFILE) {
    Start-Process -FilePath "PowerShell" -Wait -WindowStyle Minimized -Verb runas -ArgumentList $TMP_CMDFILE
    $null = Remove-Item $TMP_CMDFILE
}
else {
    Write-Host "Error creating temporary command file $TMP_CMDFILE`nAborting installation"
    exit 1
}

#re-start caché instance in order to read the license correctly
Write-Host "$(Get-TimeStamp) Installation done, restarting Caché"
try {
    Start-Process $INSTALL_DIR\bin\ccontrol.exe -Wait -NoNewWindow -ArgumentList "stopstart $CACHE_CONFIG"
}
catch {
    Write-Host "`n$(Get-TimeStamp) Error restarting $CACHE_CONFIG"
    exit 1
}

if (Test-Path $DEST_DIR`\*.ps1) {
    if (-not (Test-Path $SCRIPT_BASE)) {
        $null = New-Item -Type Directory -Path $SCRIPT_BASE
    }

    if (Test-Path $SCRIPT_BASE) {
        $null = Remove-Item -Path ${$SCRIPT_BASE}*.ps1  #cleanup directory
        $null = Copy-Item -Path ${DEST_DIR}\*.ps1 -Destination $SCRIPT_BASE
    }
}

$RELEASE_ZIP_REPO = $installationFiles.files.release
$errorFound = & $GET_FILE $RELEASE_ZIP_REPO $RELEASE_DIR
if ($errorFound) {
    Write-Host "Error copying Releasebuilder files"
    Write-Host "Please try to copy them manually"
    Write-Host "Source: $RELEASE_ZIP_REPO"
    Write-Host "Target: $RELEASE_DIR"
}

#check if installation succeeded
Write-Host "`n$(Get-TimeStamp) Installation finished`n"
Write-Host "Summary:"
Write-Host "========`n"
$result = Select-String -Path $INSTALL_LOG -Pattern succeeded
if ($result) {
    $result="successfully"
    $exitStatus = 0
    $path = [System.Environment]::GetEnvironmentVariable(('path','user'))
    #Path-variable is updated on new session only. If Path-variable already contains reference to caché then it isn't modified
    #Path variable must be set no matter if updated because on new session it will be loaded
   
    if (-Not $path) {  #$path variable is null or empty
        $path += ";$INSTALL_DIR\bin;"
    }
    if (-Not($path.contains($INSTALL_DIR))) {
        $path += ";$INSTALL_DIR\bin;"
    }
    [System.Environment]::SetEnvironmentVariable('PATH',$path,'user')
    & $INSTALL_DIR`\bin\csystray  #starting caché cube in systray
}
else {
    $result='with errors'
    $exitStatus = 1
}
Write-Host "Caché-Instance $CACHE_CONFIG installed $result"

if ($exitStatus -eq 0) {
    $pgDir = $env:ProgramFiles
    if (-not(Test-Path "$pgDir\7-Zip")) {
        Write-Host '7-Zip not available'
    }
    Write-Host 'Variable PATH has been extended to find Caché-binaries'
    Write-Host "Variable cache_db points to $DATA_DIR"
    Write-Host 'Username HLADEV created for daily work'
    Write-Host 'Username HLAADM created for administrative tasks'
    Write-Warning 'Passwords are identical with username. Please change as soon as possible'
    Write-Host -ForegroundColor Yellow "`nYou should reboot this computer in order to access environment variable CACHE_DB properly"
    Remove-Item -Path $DEST_DIR -Recurse
}
"=" * 89

exit $exitStatus
