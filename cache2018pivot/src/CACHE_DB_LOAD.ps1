﻿# CACHE_DB_LOAD.ps1 / Run release installer
# Arguments:
# 1: Name of caché instance
# 2: Hard-Coded: INSTALL | VERSION | FALLBACK
# 3: Caché namespace
#
$scriptName = $MyInvocation.MyCommand.Name
if ($args.Count -lt 3) {
    "`nUsage: $scriptName arg1 arg2 arg3"
    "arg1 = Name of caché instance"
    "arg2 = Some of FALLBACK, INSTALL, VERSION"
    "arg3 = Caché namespace`n"
    exit 1
}
$scriptNameShort = $scriptName.Substring(0,$scriptName.Length - 4)
$cacheInstance = $args[0]
$action = $args[1].ToUpper()
$cacheNamespace = $args[2]
$module = $args[3]
$version = $args[4]
$delete = $args[5]
$core = $args[6]
$logLevel = $args[7]
$fallBack = $args[8]

if (-Not(Test-Path "C:\Intersystems\Cache\$cacheInstance")) {
    "Installationsverzeichnis der Caché-Instanz $cacheInstance existiert nicht"
    exit 1
}

if ( (-Not($action.Equals("INSTALL"))) -and (-Not($action.Equals("FALLBACK"))) -and (-Not($action.Equals("VERSION"))) ) {
    "Wrong specification of action argument"
    "Only FALLBACK, INSTALL or VERSION supported"
    exit 1
}

$currentUser = $env:USERNAME
$logFileDir = "D:\$cacheInstance\Log\"
$logFile = "$logFileDir$scriptNameShort`_$currentUser`.log"
$errFile = "$logFileDir$scriptNameShort`_$currentUser`.err"
$infFile = "$logFileDir$scriptNameShort`_$currentUser`.info"
if (-not(Test-Path "$logFileDir")) {
    "Log-directory $logFileDir doesn't exist"
    exit 1
} 
else {
    Remove-Item -Path $logFile -ErrorAction SilentlyContinue
    Remove-Item -Path $errFile -ErrorAction SilentlyContinue
    Remove-Item -Path $infFile -ErrorAction SilentlyContinue
}


"Logfile : $logFile"
"--------------------------------------"
"HOST     : $cacheInstance"
"MODE     : $action"
"NAMESPACE: $cacheNamespace"
"MODULE   : $module"
"VERSION  : $version"
"DELETE   : $delete"
"CORE     : $core"
"LOGLEVEL : $logLevel"
"FALLBACK : $fallBack"
$quotes=""""""""""""""""
&C:\InterSystems\Cache\$cacheInstance\bin\csession $cacheInstance -U $cacheNamespace "%LOADWIN^%HLAIMP(${quotes}${logFile}${quotes},${quotes}${action}${quotes},${quotes}${cacheNamespace}${quotes},${quotes}${module}${quotes},${quotes}${version}${quotes},${quotes}${delete}${quotes},${quotes}${core}${quotes},${quotes}${logLevel}${quotes},${quotes}${fallBack}${quotes})"
exit 0

